<?php

namespace App\Controller\Api;

use App\Entity\Group;
use Doctrine\Common\Collections\Collection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class UsersByGroupController extends AbstractController
{
    public function __invoke(Group $group): Collection
    {
        return $group->getUsers();
    }
}