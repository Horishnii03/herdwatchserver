<?php

namespace App\EventSubscriber;

use App\Entity\Group;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PreRemoveEventArgs;
use Doctrine\ORM\Events;

class GroupSubscriber implements EventSubscriberInterface
{

    public function __construct(
        private EntityManagerInterface $entityManager
    )
    {
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::preRemove
        ];
    }

    /**
     * @throws \Exception
     */
    public function preRemove(PreRemoveEventArgs $args): void
    {
        /** @var Group $group */
        $group = $args->getObject();
        if (!$this->supports($group)) return;
        foreach ($group->getUsers() as $user) {
            $group->removeUser($user);
        }

        $this->entityManager->persist($group);
        $this->entityManager->flush();
    }

    private function supports(object $object): bool
    {
        return $object::class === Group::class;
    }
}